# Abstract REST API handler

Abstract to be extended and used in API implementations.
The way to use:

1. Extend `AbstractApi` class
2. Extend `ApiConfiguration` to replace urls or configure an instance
3. To do requests use `AbstractApi.request()` method (it returns `RequestHandler` object)
4. By needs - extend `GenericApiContext` to rule out contexts in api calls/handlers

Good practice is to define API groups in `AbstractApi.map`. An example is shown in `examples/DummyApi.php`