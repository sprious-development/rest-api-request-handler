<?php

namespace Blazing\Common\RestApiRequestHandler;

use Monolog\Logger;
use ErrorException;

class GenericApiConfiguration
{

    protected $protocol = 'http';

    protected $host = '';

    protected $url = '/api/path/should/be/defined/in/configuration';

    protected $apiToken;

    protected $logger;

    protected $defaultRequestOptions = [
        'timeout'      => 30,
        'verifyHost'   => false,
        'verifyPeer'   => false,
        'maxRedirects' => 5,
        'retry'        => 3,
        'sendMasterRequestParameters' => true
    ];

    protected $requestOptions = [];

    protected $headers = [];
    /**
     * @param string $host
     * @param string $url
     * @param null $protocol
     * @return static
     */
    public static function build($host, $url, $protocol = null)
    {
        $self = new static();

        if ($host) {
            $self->setHost($host);
        }

        if ($url) {
            $self->setUrl($url);
        }

        if ($protocol) {
            $self->setProtocol($protocol);
        }

        return $self;
    }

    public function __construct()
    {
        $this->requestOptions = array_merge($this->defaultRequestOptions, $this->requestOptions);
    }

    /**
     * Get protocol
     *
     * @return string
     * @throws ErrorException
     */
    public function getProtocol()
    {
        if (!$this->protocol) {
            throw new ErrorException('Protocol is not configured!');
        }

        return $this->protocol;
    }

    /**
     * Set protocol
     *
     * @param string $protocol
     * @return $this
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;

        return $this;
    }

    /**
     * Get host
     *
     * @return string
     * @throws ErrorException
     */
    public function getHost()
    {
        if (!$this->host) {
            throw new ErrorException('Host is not configured!');
        }

        return $this->host;
    }

    /**
     * Set host
     *
     * @param string $host
     * @return $this
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     * @throws ErrorException
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get apiToken
     *
     * @return mixed
     * @throws ErrorException
     */
    public function getApiToken()
    {
        return $this->apiToken;
    }

    /**
     * Set apiToken
     *
     * @param mixed $apiToken
     * @return $this
     */
    public function setApiToken($apiToken)
    {
        $this->apiToken = $apiToken;

        return $this;
    }

    /**
     * Get logger
     *
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Set logger
     *
     * @param Logger $logger
     * @return $this
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * Get request options
     *
     * @return array
     */
    public function getRequestOptions()
    {
        return $this->requestOptions;
    }

    /**
     * Get request option or default value
     *
     * @param $option
     * @param null $default
     * @return mixed|null
     */
    public function getRequestOption($option, $default = null)
    {
        return isset($this->requestOptions[ $option ]) ? $this->requestOptions[ $option ] : $default;
    }

    /**
     * @param $option
     * @param $value
     * @return $this
     */
    public function setRequestOption($option, $value)
    {
        if (isset($this->requestOptions[ $option ])) {
            $this->requestOptions[ $option ] = $value;
        }

        return $this;
    }

    /**
     * @param null $name
     * @return array|mixed
     * @throws \Exception
     */
    public function getHeaders($name = null)
    {
        if ($name) {
            if (!isset($this->headers[$name])) {
                throw new \Exception("Header with name: {$name} is not configured");
            }
            return $this->headers[$name];
        }
        return $this->headers;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function setHeader($name, $value)
    {
        $this->headers[$name] = $value;
        return $this;
    }
}
