<?php

namespace Blazing\Common\RestApiRequestHandler\Api;

abstract class AbstractApi
{
    /**
     * @var \Blazing\Common\RestApiRequestHandler\AbstractApi
     */
    protected $api;

    public function __construct(\Blazing\Common\RestApiRequestHandler\AbstractApi $api)
    {
        $this->api = $api;
    }
}
