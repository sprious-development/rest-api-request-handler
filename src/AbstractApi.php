<?php

namespace Blazing\Common\RestApiRequestHandler;

use ErrorException;

abstract class AbstractApi
{
    /**
     * @var GenericApiConfiguration
     */
    protected $configuration;

    /**
     * @var GenericApiContext
     */
    protected $context;

    /**
     * @var RequestHandler
     */
    protected $requestHandler;

    // Classes map
    protected $map = [];

    protected $loaded = [];

    public function __construct(GenericApiConfiguration $configuration)
    {
        $this->configuration = $configuration;
        $this->clearContext();
    }

    public function __call($name, $arguments)
    {
        if (!empty($this->map[$name])) {
            return $this->buildApi($name);
        }

        throw new \RuntimeException("Method \"$name\" doesn't exist");
    }

    protected function buildApi($type)
    {
        if (!empty($this->loaded[$type])) {
            return $this->loaded[$type];
        }

        if (empty($this->map[$type])) {
            throw new ErrorException("Api \"$type\" is not defined");
        }

        $class = $this->map[$type];

        return $this->loaded[$type] = new $class($this);
    }

    // Request handler

    /**
     * @return RequestHandler
     */
    public function request()
    {
        if (!$this->requestHandler) {
            $this->requestHandler = new RequestHandler($this->configuration);
        }

        return $this->requestHandler;
    }

    public function setRequestHandler(RequestHandler $requestHandler)
    {
        $this->requestHandler = $requestHandler;

        return $this;
    }

    public function getConfiguration()
    {
        return $this->configuration;
    }

    public function setContext(GenericApiContext $context)
    {
        $this->context = $context;
    }

    public function clearContext()
    {
        $this->context = new GenericApiContext();
    }

    public function getContext()
    {
        return $this->context;
    }
}
