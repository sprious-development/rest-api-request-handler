<?php

namespace Blazing\Common\RestApiRequestHandler;

use Blazing\Common\RestApiRequestHandler\Exception\BadRequestException;
use Blazing\Common\RestApiRequestHandler\Exception\RequestsException;
use Buzz\Browser;
use Buzz\Exception\RequestException;
use Buzz\Message\Response;
use ErrorException;
use Blazing\Logger\Logger;

class RequestHandler
{

    /**
     * @var GenericApiConfiguration
     */
    protected $configuration;

    public function __construct(GenericApiConfiguration $configuration)
    {
        $this->configuration = $configuration;
    }

    /* @deprecated */
    public function get($path, array $data = [], array $headers = [])
    {
        return $this->request($path, $data, 'GET', $headers);
    }

    /* @deprecated */
    public function put($path, array $data = [], array $headers = [])
    {
        return $this->request($path, $data, 'PUT', $headers);
    }

    /* @deprecated */
    public function post($path, array $data = [], array $headers = [])
    {
        return $this->request($path, $data, 'POST', $headers);
    }

    /* @deprecated */
    public function delete($path, array $data = [], array $headers = [])
    {
        return $this->request($path, $data, 'DELETE', $headers);
    }

    /* @deprecated Use doRequest instead */
    public function request($path, array $data = [], $method = 'POST', array $headers = [])
    {
        return $this->doRequest($path, $data, $method, $headers)->getContent();
    }

    public function doGet($path, array $data = [], array $headers = [])
    {
        return $this->doRequest($path, $data, 'GET', $headers);
    }

    public function doPut($path, array $data = [], array $headers = [])
    {
        return $this->doRequest($path, $data, 'PUT', $headers);
    }

    public function doPost($path, array $data = [], array $headers = [])
    {
        return $this->doRequest($path, $data, 'POST', $headers);
    }

    public function doDelete($path, array $data = [], array $headers = [])
    {
        return $this->doRequest($path, $data, 'DELETE', $headers);
    }

    /**
     * @param $path
     * @param array $data
     * @param string $method
     * @param array $headers
     * @return Response
     * @throws BadRequestException
     * @throws ErrorException
     * @throws RequestsException
     */
    public function doRequest($path, array $data = [], $method = 'POST', array $headers = [])
    {
        // Replace path variables
        foreach ($data as $key => $value) {
            if ($value and false !== strpos($path, '{' . $key . '}')) {
                $path = str_replace('{' . $key . '}', (string) $value, $path);
                unset($data[$key]);
            }
        }

        // Add master request track
        $logger = $this->configuration->getLogger();
        if ($logger instanceof Logger && $this->configuration->getRequestOption('sendMasterRequestParameters', true)) {
            $data += $logger->prepareMasterRequestParameter();
        }

        // Determine URI
        $relativeUrl = $this->configuration->getUrl();
        if ($relativeUrl) {
            $relativeUrl = '/' . trim($relativeUrl, '/');
        }
        $url = sprintf('%s://%s%s/%s',
            $this->configuration->getProtocol(),
            $this->configuration->getHost(),
            $relativeUrl,
            trim($path, '/'));

        // Pass xdebug cookies
        // if (!empty($_COOKIE['XDEBUG_SESSION'])) {
        //    $headers[] = "Cookie: XDEBUG_SESSION={$_COOKIE['XDEBUG_SESSION']}";
        //}

        // Auth header
        if ($this->configuration->getApiToken()) {
            $headers[] = 'Auth-Token: ' . $this->configuration->getApiToken();
        }

        // Add headers
        $headers = array_merge($headers, $this->configuration->getHeaders());

        $json = false;
        if (in_array('Content-Type: application/json', $headers)) {
            $json = true;
        }

        $browser = new Browser();
        $browser->getClient()->setTimeout($this->configuration->getRequestOption('timeout'));
        $browser->getClient()->setVerifyHost($this->configuration->getRequestOption('verifyHost'));
        $browser->getClient()->setVerifyPeer($this->configuration->getRequestOption('verifyPeer'));
        $browser->getClient()->setMaxRedirects($this->configuration->getRequestOption('maxRedirects'));
        $tries = $this->configuration->getRequestOption('retry', 3);

        try {
            while (true) {
                try {
                    /** @var Response $response */
                    if ($json) {
                        $response = $browser->call($url, $method, $headers, json_encode($data));
                    } else {
                        $response = $browser->submit($url, $data, $method, $headers);
                    }
                    $content = $response->getContent();

                    if (200 !== $response->getStatusCode() and '{' != substr($content, 0, 1)) {
                        throw new RequestException(sprintf('Error %s: %s', $response->getStatusCode(), substr($content, 0, 250)));
                    }

                    break;
                }
                catch (RequestException $e) {
                    $tries--;

                    // Stop trying
                    if ($tries <= 0) {
                        // Rethrow an exception
                        throw $e;
                    }

                    if ($this->configuration->getLogger()) {
                        $this->configuration->getLogger()->warn("API: Request exception $method:$path", [
                            'error' => $e->getMessage(),
                            'triesLeft' => $tries,
                            'response' => !empty($response) ? substr($response->getContent(), 0, 250) : null
                        ]);
                    }

                    sleep(1);
                }
            }

            if (empty($content)) {
                throw new ErrorException('Empty content in response');
            }

            $parsedContent = @json_decode($content, true);
            if (!$parsedContent) {
                throw new ErrorException('No data in response, content: ' . $content);
            }

            if (!empty($parsedContent['status']) and 'error' == $parsedContent['status'] or isset($parsedContent['error'])) {
                throw new BadRequestException(
                    !empty($parsedContent['message']) ? $parsedContent['message'] : $content,
                    $parsedContent,
                    $content,
                    $response->getStatusCode()
                );
            }
            $response->setContent($parsedContent);
        }
        catch (BadRequestException $e) {
            if ($this->configuration->getLogger()) {
                $this->configuration->getLogger()->warn("API: Request exception $method:$path", [
                    'error' => $e->getMessage(),
                    'data' => $e->getData(),
                    'url'  => $url,
                    'requestData' => $data,
                ]);
            }

            throw $e;
        }
        catch (\Exception $e) {
            if ($this->configuration->getLogger()) {
                $this->configuration->getLogger()->warn("API: Request error $method:$path", [
                    'error' => $e->getMessage(),
                    'url'   => $url,
                    'requestData' => $data,
                ]);
            }
            throw new RequestsException("Request error ($method:$path): " . $e->getMessage());
        }

        if ($this->configuration->getLogger()) {
            $logData = $parsedContent;

            // Cut large data
            if (!empty($logData[ 'list' ]) and 10 < count($logData[ 'list' ])) {
                $logData[ 'list' ] = array_merge(array_slice(array_values($logData[ 'list' ]), 0, 10), ['large data...']);
            }

            $this->configuration->getLogger()->debug("API: Response $method:$path", [
                'parameters' => $data,
                'response'   => $logData,
                'url'        => $url
            ]);
        }

        return $response;
    }
}
