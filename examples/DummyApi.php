<?php

use Blazing\Common\RestApiRequestHandler\AbstractApi;

class DummyApi extends AbstractApi
{
    protected $map = [
        'dummy' => DummyApiGroup::class
    ];

    /**
     * @return DummyApiGroup
     */
    public function dummy()
    {
        return $this->buildApi('dummy');
    }
}
